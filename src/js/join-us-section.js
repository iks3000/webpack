export const events = () => {
    window.addEventListener('load', () => {
        console.log('Page loaded!');

        const joinSection = document.querySelector('#join')
        if (joinSection !== null) join.style.display = 'block';

        const formDetails = document.querySelector('#formSubscribe');
        const inputEmail = document.querySelector('#inputSubscribe');
        if (inputEmail !== null) {
            formDetails.addEventListener('submit', (e) => {
                e.preventDefault();

                console.log('Email: ' + inputEmail.value);
                inputEmail.value = '';
            }, false)
        }
    });
}

class Creator {
    constructor(title, button) {
        this.title = title;
        this.button = button;
    }
}

const standardType = new Creator("Join Our Program", "Subscribe");
const advancedType = new Creator("Join Our Advanced Program", "Subscribe to Advanced");

export function changeContentStandard() {
    window.addEventListener("load", () => {
        getTitle.innerHTML = standardType.title;
        getButton.innerHTML = standardType.button;
    });
}

export function changeContentAdvance() {
    window.addEventListener("load", () => {
        getTitle.innerHTML = advancedType.title;
        getButton.innerHTML = advancedType.button;
    });
}

export const remove = () => {
    joinSection.remove();
};