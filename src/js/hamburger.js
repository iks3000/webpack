export const hamburger = () => {
    const hamburger = document.querySelector('.hamburger');
    const navMenu = document.querySelector('.navList');

    hamburger.addEventListener('click', mobileMenu);

    function mobileMenu() {
        hamburger.classList.toggle('active');
        navMenu.classList.toggle('active');
    }

    const navLink = document.querySelectorAll('.navLink');

    navLink.forEach(nav => nav.addEventListener('click', closeMenu));

    function closeMenu() {
        hamburger.classList.remove('active');
        navMenu.classList.remove('active');
    }
}
