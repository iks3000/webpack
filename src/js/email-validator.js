
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export const validate = () => {
    const buttonForm = document.querySelector('#joinButton');
    const inputEmail = document.querySelector('#inputSubscribe');

    buttonForm.addEventListener('click', () => {
        const emailValue = inputEmail.value;
        const inputEnding = emailValue.substring(emailValue.indexOf('@') + 1);
        const valid = VALID_EMAIL_ENDINGS.some(value => value === inputEnding);
        console.log(valid);
    }, false)
}
